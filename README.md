# AIA Performance test
Document for AIA Performance test project

## Install Jmeter
- Step 1: Download jdk14 at [here](https://www.oracle.com/java/technologies/javase/jdk14-archive-downloads.html) and double click to file to install it.
- Step 2: Add `JAVA_HOME` to system variable.
- Step 3: Download jmeter at [here](https://jmeter.apache.org/download_jmeter.cgi), extract to directory.
- Step 4: Add `JMETER_HOME` with value is path of jmeter directory to system variable.
- <details>
    <summary>Step 5: Install plugin</summary>

    Go to [Jmeter plugin page](https://jmeter-plugins.org/), search, download, extract and copy plugins bellow to jmeter directory.
    - Parallel Controller & Sampler
    - Custom Thread Groups
    - Command-Line Graph Plotting Tool
    - Variables from CSV File
    </details>
- Step 6: Open cmd on bin directory and run with command `jmeter`


## Run test script
The first we need to [install Jmeter](#Install-Jmeter)
- Step 1: download this project to local
- Step 2: Open project with jmeter
- <details>
    <summary>Step 4: Click run button</summary>

    ![image](./static_resources/jmeter-run-button.png)
    </details>

## Configuration test run

1. Data test: Username and password save in file `datatest.csv`.
2. <details>
    <summary>Configuration test (Number user, holdload, ...), these variables defined in <code>User Defined Variables</code> in test plan.</summary>

    ![image](./static_resources/jmeter-config-list_val.png)
    </details>