REM @echo off

set pathReport=%~dp0output

for /F "delims=" %%G IN ('powershell "gci -Path %pathReport% | sort LastWriteTime | Select-Object name"') do set outPath=%%G
set pathReport=%pathReport%\%outPath%
for /F "delims=" %%G IN ('powershell "gci -Path %pathReport% | sort LastWriteTime | Select-Object name"') do set outPath=%%G
set pathReport=%pathReport%\%outPath%
echo %pathReport%
call %JMETER_HOME%\bin\jmeter -g %pathReport%\all\summary.csv -o %pathReport%\all\html
call java -jar %~dp0jtl-excel\Jtl-excel-1.0-SNAPSHOT.jar %pathReport%\all\summary.xml %pathReport%\all\report 30000
call start %windir%\explorer.exe %pathReport%\all